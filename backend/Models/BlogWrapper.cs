using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace backend.Models
{

    public class Category
    {
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public List<BlogWrapper>? BlogWrappers { get; set; }
    }
    public class User
    {
        public int UserId { get; set; }
        [MaxLength(30)]
        public string FirstName { get; set; }
        [MaxLength(30)]
        public string LastName { get; set; }
        [MaxLength(50)]
        [EmailAddress]
        public string EmailId { get; set; }
        public string? ImageUrl { get; set; }
        public long MobileNumber { get; set; }
        [JsonIgnore]
        public List<BlogWrapper>? BlogWrappers { get; set; }
        [JsonIgnore]
        public List<Like>? Likes { get; set; }
        [JsonIgnore]
        public List<Comment>? Comments { get; set; }
        [JsonIgnore]
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
    }

    public class BlogWrapper
    {
        public int BlogWrapperId { get; set; }
        public string Title { get; set; }
        public string Metadata { get; set; }
        public string permalink { get; set; }
        public string ImageUrl { get; set; }
        [JsonIgnore]
        public Category? Category { get; set; }
        public int CategoryId { get; set; } // Foreign key to Category
        public int UserId { get; set; } // User who wrote the BlogWrapper
        [JsonIgnore]
        public List<Like>? Likes { get; set; }
        [JsonIgnore]
        public List<Comment>? Comments { get; set; }
        public List<Blog>? Blogs { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
    }

    public class Blog
    {
        public int BlogId { get; set; }
        public int BlogOrderId { get; set; }
        public string Text { get; set; }
        public string ImageUrl { get; set; }
        public BlogWrapper BlogWrapper { get; set; }
        public int BlogWrapperId { get; set; }
    }

    public class Like
    {
        public int LikeId { get; set; }
        public bool IsLiked { get; set; } = false;
        public int BlogWrapperId { get; set; } // Foreign key to BlogWrapper
        public BlogWrapper BlogWrapper { get; set; }
        public int UserId { get; set; } // Foreign key to User
        public User User { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
    }

    public class Comment
    {
        public int CommentId { get; set; }
        public string Text { get; set; }
        public int BlogWrapperId { get; set; } // Foreign key to BlogWrapper
        public BlogWrapper BlogWrapper { get; set; }
        public int UserId { get; set; } // Foreign key to User
        public User User { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}