namespace backend.DTO
{
    public class BaseRequestModel
    {

    }

    public class BaseResponseModel
    {
        public string Message {get;set;}
        public bool Status { get; set; }
        public object? Data {get;set;}
    }
}