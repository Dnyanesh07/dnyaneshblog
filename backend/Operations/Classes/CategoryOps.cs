using backend.DTO;
using backend.Helper;
using backend.Models;
using backend.Operations.Interfaces;
using backend.Utility;
using Microsoft.EntityFrameworkCore;

namespace backend.Operations.Classes
{
    public class CategoryOps : ICategoryOps
    {
        private readonly DbHelper dbHelper;

        public CategoryOps(DbHelper dbHelper)
        {
            this.dbHelper = dbHelper;
        }

        public async Task<BaseResponseModel> AddCategoryOpsAsync(Category category)
        {
            List<Category> c = dbHelper.Categories.Where(c => c.Title.ToLower() == category.Title.ToLower()).ToList();
            if (c.Count > 0)
                return CommonObjectMapper.CreateBaseResposeModel("category already exist", false, c[0]);
            var data = await dbHelper.Categories.AddAsync(category);
            await dbHelper.SaveChangesAsync();
            return CommonObjectMapper.CreateBaseResposeModel("Category added successfully", true, data.Entity);

        }

        public async Task<BaseResponseModel> GetAllCategoriesOpsAsync()
        {
            List<Category> c = dbHelper.Categories.AsNoTracking().ToList();

            return CommonObjectMapper.CreateBaseResposeModel("Category added successfully", true, c);

        }

        public async Task<BaseResponseModel> GetBlogWrapperByCategoriesOpsAsync(string Title)
        {
            List<Category> c = dbHelper.Categories.AsNoTracking().Where(c=>c.Title.ToLower()==Title.ToLower()).Include(c => c.BlogWrappers).ToList();
            return CommonObjectMapper.CreateBaseResposeModel("Category added successfully", true, c);
        }

    }
}