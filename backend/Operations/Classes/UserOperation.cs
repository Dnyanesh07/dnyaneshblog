using backend.Operations.Interfaces;
using backend.Helper;
using backend.Models;
using Microsoft.EntityFrameworkCore;
using backend.Utility;
using backend.DTO;

namespace backend.Operations.Classes
{
    public class UserOperation : IUserOperation
    {
        private readonly DbHelper dbHelper;
        public UserOperation(DbHelper dbHelper)
        {
            this.dbHelper = dbHelper;
        }

        public async Task<BaseResponseModel> CallUpdateUserOpsAsync(User user)
        {

            User? a = await dbHelper.Users.FirstOrDefaultAsync(u => u.UserId == user.UserId);
            if (a == null)
                return CommonObjectMapper.CreateBaseResposeModel("Invalid UserID", false, a);

            a.MobileNumber = user.MobileNumber;

            await dbHelper.SaveChangesAsync();

            return CommonObjectMapper.CreateBaseResposeModel("User updated successfully", true, a);
        }

        public async Task<BaseResponseModel> CallAddUserOpsAsync(User user)
        {

            User? a = await dbHelper.Users.FirstOrDefaultAsync(u => u.EmailId == user.EmailId);
            if (a != null)
                return CommonObjectMapper.CreateBaseResposeModel("Email Id already exist", false, a);

            var b = await dbHelper.Users.AddAsync(user);

            await dbHelper.SaveChangesAsync();

            return CommonObjectMapper.CreateBaseResposeModel("User Added successfully", true, b.Entity);
        }
    }
}