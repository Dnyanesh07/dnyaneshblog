using backend.DTO;
using backend.Helper;
using backend.Models;
using backend.Operations.Interfaces;
using backend.Utility;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace backend.Operations.Classes
{
    public class BlogOps : IBlogOps
    {
        private readonly DbHelper dbHelper;
        public BlogOps(DbHelper dbHelper)
        {
            this.dbHelper = dbHelper;
        }

        public async Task<BaseResponseModel> AddSingleBlogOpsAsync(Blog blog)
        {
            var b = await dbHelper.Blogs.AddAsync(blog);
            await dbHelper.SaveChangesAsync();
            return CommonObjectMapper.CreateBaseResposeModel("Blog added successfully", true, b.Entity);
        }

        public async Task<BaseResponseModel> DeleteSingleBlogOpsAsync(int BlogId)
        {
            Blog? entityEntry = await dbHelper.Blogs.FirstOrDefaultAsync(blog => blog.BlogId == BlogId);
            if (entityEntry == null)
            {
                return CommonObjectMapper.CreateBaseResposeModel("No blog added by user to remove", false);
            }
            dbHelper.Blogs.Remove(entityEntry);
            await dbHelper.SaveChangesAsync();
            return CommonObjectMapper.CreateBaseResposeModel("Blog deleted successfully", true, entityEntry);
        }
        public async Task<BaseResponseModel> AddAllBlogsOpsAsync(List<Blog> blogs)
        {
            await dbHelper.Blogs.AddRangeAsync(blogs);
            await dbHelper.SaveChangesAsync();
            return CommonObjectMapper.CreateBaseResposeModel("All blogs added successfully", true);
        }

        public async Task<BaseResponseModel> DeleteAllBlogsOpsAsync(int BlogId)
        {
            var entityEntry = dbHelper.Blogs.Where(blog => blog.BlogWrapperId == BlogId).ToList().RemoveAll(blog => blog.BlogWrapperId == BlogId);

            await dbHelper.SaveChangesAsync();
            return CommonObjectMapper.CreateBaseResposeModel("Blog deleted successfully", true, entityEntry);
        }
        public async Task<BaseResponseModel> GetAllBlogsOpsAsync(int blogWrapperID)
        {
            var entityEntry = dbHelper.Blogs.AsNoTracking().Where(blog => blog.BlogWrapperId == blogWrapperID).ToList();

            return CommonObjectMapper.CreateBaseResposeModel("All Blog added successfully", true, entityEntry);
        }

        public async Task<BaseResponseModel> UpdateAllBlogsOpsAsync(int blogWrapperID, List<Blog> blogs)
        {
            List<Blog> result = dbHelper.Blogs.Where(blog => blog.BlogWrapperId == blogWrapperID).ToList();

            if (result == null)
                return CommonObjectMapper.CreateBaseResposeModel("No blog added by user to update", false);
            for (int i = 0; i < result.Count; i++)
            {
                result[i].Text = blogs[i].Text;
                result[i].ImageUrl = blogs[i].ImageUrl;
            }

            await dbHelper.SaveChangesAsync();

            return CommonObjectMapper.CreateBaseResposeModel("All Blog added successfully", true, result);
        }
    }
}