using backend.DTO;
using backend.Helper;
using backend.Models;
using backend.Operations.Interfaces;
using backend.Utility;
using Microsoft.EntityFrameworkCore;

namespace backend.Operations.Classes
{
    public class BlogWrapperOps : IBlogWrapperOps
    {
        private readonly DbHelper dbHelper;
        public BlogWrapperOps(DbHelper dbHelper)
        {
            this.dbHelper = dbHelper;
        }

        public async Task<Like> AddLikeOpsAsync(Like like)
        {
            var entityEntry = await dbHelper.Likes.AddAsync(like);
            await dbHelper.SaveChangesAsync();

            // Access the entity using the Entity property of EntityEntry
            Like data = entityEntry.Entity;

            return data;
        }

        public async Task<BaseResponseModel> RemoveLikeOpsAsync(Like like)
        {
            var entityEntry = await dbHelper.Likes.FirstOrDefaultAsync(l => l.UserId == like.UserId && l.BlogWrapperId == like.BlogWrapperId);

            if (entityEntry == null)
                return CommonObjectMapper.CreateBaseResposeModel("No like given by user to remove", false);

            dbHelper.Likes.Remove(entityEntry);
            await dbHelper.SaveChangesAsync();

            return CommonObjectMapper.CreateBaseResposeModel("Like removed successfully");
        }

        public async Task<Comment> AddCommentOpsAsync(Comment comment)
        {
            var entityEntry = await dbHelper.Comments.AddAsync(comment);
            await dbHelper.SaveChangesAsync();

            // Access the entity using the Entity property of EntityEntry
            Comment data = entityEntry.Entity;

            return data;
        }

        public async Task<List<Comment>> GetAllCommentOpsAsync(int Id)
        {
            List<Comment> entityEntry = dbHelper.Comments.AsNoTracking().Where(comment => comment.BlogWrapperId == Id).ToList();

            return entityEntry;
        }

        public async Task<BaseResponseModel> UpdateCommentOpsAsync(Comment comment)
        {
            Comment? entityEntry = await dbHelper.Comments.FirstOrDefaultAsync(comment => comment.CommentId == comment.CommentId);

            if (entityEntry == null)
                return CommonObjectMapper.CreateBaseResposeModel("No comment added by user to update", false);

            entityEntry.Text = comment.Text;
            await dbHelper.SaveChangesAsync();
            return CommonObjectMapper.CreateBaseResposeModel("comment updated successfully", true, entityEntry);

        }

        public async Task<BaseResponseModel> DeleteCommentOpsAsync(int Id)
        {
            Comment? entityEntry = await dbHelper.Comments.FirstOrDefaultAsync(comment => comment.CommentId == Id);
            if (entityEntry == null)
            {
                return CommonObjectMapper.CreateBaseResposeModel("No comment added by user to remove", false);
            }
            dbHelper.Comments.Remove(entityEntry);
            await dbHelper.SaveChangesAsync();
            return CommonObjectMapper.CreateBaseResposeModel("Comment deleted successfully", true, entityEntry);
        }

        public async Task<BaseResponseModel> AddBlogWrapperAsync(BlogWrapper blogWrapper)
        {
            var result = await dbHelper.BlogWrappers.AddAsync(blogWrapper);
            await dbHelper.SaveChangesAsync();

            return CommonObjectMapper.CreateBaseResposeModel("BlogWrapperAdded successfully", true, result.Entity);
        }
        public async Task<BaseResponseModel> DeleteBlogWrapperAsync(int Id)
        {
            BlogWrapper? entityEntry = await dbHelper.BlogWrappers.FirstOrDefaultAsync(b => b.BlogWrapperId == Id);
            if (entityEntry == null)
            {
                return CommonObjectMapper.CreateBaseResposeModel("No Blog wrapper added by user to remove", false);
            }
            dbHelper.BlogWrappers.Remove(entityEntry);
            await dbHelper.SaveChangesAsync();
            return CommonObjectMapper.CreateBaseResposeModel("Blog Wrapper deleted successfully", true, entityEntry);
        }
        public async Task<BaseResponseModel> UpdateBlogWrapperAsync(BlogWrapper blogWrapper)
        {
            BlogWrapper? entityEntry = await dbHelper.BlogWrappers.FirstOrDefaultAsync(b => b.BlogWrapperId == blogWrapper.BlogWrapperId);
            if (entityEntry == null)
            {
                return CommonObjectMapper.CreateBaseResposeModel("No Blog wrapper added by user to update", false);
            }
            entityEntry.Title = blogWrapper.Title;
            entityEntry.Metadata = blogWrapper.Metadata;
            entityEntry.permalink = blogWrapper.permalink;
            entityEntry.Category = blogWrapper.Category;
            entityEntry.ImageUrl = blogWrapper.ImageUrl;

            await dbHelper.SaveChangesAsync();
            return CommonObjectMapper.CreateBaseResposeModel("Blog Wrapper updated successfully", true, entityEntry);
        }

        public async Task<BaseResponseModel> GetBlogWrapperOpsAsync(int startCount)
        {
            List<BlogWrapper> blogWrappers = dbHelper.BlogWrappers.Include(l=>l.Blogs).OrderByDescending(s => s.CreatedAt).Skip(startCount).Take(30).ToList();

            return CommonObjectMapper.CreateBaseResposeModel("Blog Wrapper added successfully", true, blogWrappers);
        }
        public async Task<BaseResponseModel> GetCompleteBlogbyPermalinkOpsAsync(string permalink)
        {
            var blogWrapper = dbHelper.BlogWrappers.Where(b => b.permalink.ToLower() == permalink.ToLower()).Include(b => b.Blogs);
            return CommonObjectMapper.CreateBaseResposeModel("Complete Blog by permalink added successfully", true, blogWrapper);
        }

        public async Task<BaseResponseModel> GetCompleteBlogbyBlogWrapperIdOpsAsync(string BlogWrapperId)
        {
            var blogWrapper = dbHelper.BlogWrappers.Where(b => b.BlogWrapperId == Convert.ToInt32(BlogWrapperId)).Include(b => b.Blogs);
            return CommonObjectMapper.CreateBaseResposeModel("Complete Blog by blogWrapperID added successfully", true, blogWrapper);
        }
    }
}