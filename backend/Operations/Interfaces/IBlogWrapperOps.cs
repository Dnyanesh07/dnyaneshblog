using backend.DTO;
using backend.Models;

namespace backend.Operations.Interfaces
{
    public interface IBlogWrapperOps
    {
        public Task<Like> AddLikeOpsAsync(Like like);
        public Task<BaseResponseModel> RemoveLikeOpsAsync(Like like);
        public Task<Comment> AddCommentOpsAsync(Comment comment);
        public Task<List<Comment>> GetAllCommentOpsAsync(int Id);
        public Task<BaseResponseModel> DeleteCommentOpsAsync(int Id);
        public Task<BaseResponseModel> UpdateCommentOpsAsync(Comment comment);
        public Task<BaseResponseModel> AddBlogWrapperAsync(BlogWrapper blogWrapper);
        public Task<BaseResponseModel> DeleteBlogWrapperAsync(int Id);
        public Task<BaseResponseModel> UpdateBlogWrapperAsync(BlogWrapper blogWrapper);
        public Task<BaseResponseModel> GetBlogWrapperOpsAsync(int startCount);
        public Task<BaseResponseModel> GetCompleteBlogbyPermalinkOpsAsync(string permalink);
        public Task<BaseResponseModel> GetCompleteBlogbyBlogWrapperIdOpsAsync(string BlogWrapperId);
    }
}