using backend.DTO;
using backend.Models;

namespace backend.Operations.Interfaces
{
    public interface IUserOperation
    {
        public Task<BaseResponseModel> CallUpdateUserOpsAsync(User user);
        public Task<BaseResponseModel> CallAddUserOpsAsync(User user);
    }
    
}