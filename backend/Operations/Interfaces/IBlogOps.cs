using backend.DTO;
using backend.Models;

namespace backend.Operations.Interfaces
{
    public interface IBlogOps
    {
        public Task<BaseResponseModel> AddSingleBlogOpsAsync(Blog blog);
        public Task<BaseResponseModel> DeleteSingleBlogOpsAsync(int BlogId);
        public Task<BaseResponseModel> AddAllBlogsOpsAsync(List<Blog> blogs);
        public Task<BaseResponseModel> DeleteAllBlogsOpsAsync(int BlogId);
        public Task<BaseResponseModel> GetAllBlogsOpsAsync(int blogWrapperID);
        public Task<BaseResponseModel> UpdateAllBlogsOpsAsync(int blogWrapperID, List<Blog> blogs);
    }
}