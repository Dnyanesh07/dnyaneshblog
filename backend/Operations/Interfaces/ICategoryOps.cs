using backend.DTO;
using backend.Models;

namespace backend.Operations.Interfaces
{
    public interface ICategoryOps
    {
        public Task<BaseResponseModel> AddCategoryOpsAsync(Category category);
        public Task<BaseResponseModel> GetAllCategoriesOpsAsync();
        public Task<BaseResponseModel> GetBlogWrapperByCategoriesOpsAsync(string Title);
    }
}