using backend.DTO;
using backend.Models;
using backend.Operations.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace backend.Contoller
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryOps CategoryOps;

        public CategoryController(ICategoryOps CategoryOps)
        {
            this.CategoryOps = CategoryOps;
        }

        [HttpPost("addCategory")]
        public async Task<IActionResult> AddCategory([FromBody] Category category)
        {
            BaseResponseModel result = await CategoryOps.AddCategoryOpsAsync(category);
            if (result.Status)
                return Ok(result);
            return StatusCode(503, result);
        }

        [HttpGet("getAllCategories")]
        public async Task<IActionResult> GetAllCategories()
        {
            BaseResponseModel result = await CategoryOps.GetAllCategoriesOpsAsync();
            if (result.Status)
                return Ok(result);
            return StatusCode(503, result);
        }

        [HttpGet("getBlogWrapperByCategories/{Title}")]
        public async Task<IActionResult> GetBlogWrapperByCategories([FromRoute] string Title)
        {
            BaseResponseModel result = await CategoryOps.GetBlogWrapperByCategoriesOpsAsync(Title);
            if (result.Status)
                return Ok(result);
            return StatusCode(503, result);
        }
    }
}