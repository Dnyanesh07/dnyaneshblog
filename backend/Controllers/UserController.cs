using backend.DTO;
using backend.Helper;
using backend.Models;
using backend.Operations.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace backend.Contoller
{
    [ApiController]
    [Route("[Controller]")]
    public class UserController : ControllerBase
    {
        private readonly DbHelper dbHelper;
        private readonly IUserOperation UserOperation;
        public UserController(DbHelper dbHelper, IUserOperation UserOperation)
        {
            this.dbHelper = dbHelper;
            this.UserOperation = UserOperation;
        }

        [HttpPost("addUser")]
        public async Task<IActionResult> AddUser([FromBody] User user)
        {
            var a = await UserOperation.CallAddUserOpsAsync(user);
            if (a.Status)
                return Ok(a);
            return StatusCode(503, a);
        }

        [HttpGet("getUser/{Id}")]
        public async Task<IActionResult> GetUser([FromRoute] int Id)
        {
            var a = await dbHelper.Users.FirstOrDefaultAsync(user => user.UserId == Id);
            //await dbHelper.SaveChangesAsync();
            return Ok(a);
        }

        [HttpPatch("updateUser")]
        public async Task<IActionResult> UpdateUser([FromBody] User user)
        {
            BaseResponseModel result = await UserOperation.CallUpdateUserOpsAsync(user);
            if (result.Status)
                return Ok(result);
            return StatusCode(503, result);
        }
    }
}