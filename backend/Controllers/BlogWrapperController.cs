using backend.DTO;
using backend.Models;
using backend.Operations.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace backend.Contoller
{
    [ApiController]
    [Route("[Controller]")]
    public class BlogWrapperController : ControllerBase
    {
        private readonly IBlogWrapperOps BlogWrapperOps;
        public BlogWrapperController(IBlogWrapperOps BlogWrapperOps)
        {
            this.BlogWrapperOps = BlogWrapperOps;
        }

        [HttpPost("addLike")]
        public async Task<IActionResult> AddLike([FromBody] Like like)
        {
            var result = await BlogWrapperOps.AddLikeOpsAsync(like);

            return Ok(result);
        }
        [HttpDelete("RemoveLike")]
        public async Task<IActionResult> RemoveLike([FromBody] Like like)
        {
            BaseResponseModel result = await BlogWrapperOps.RemoveLikeOpsAsync(like);
            if (result.Status)
                return Ok(result);
            return NotFound(result);
        }

        [HttpPost("addComment")]
        public async Task<IActionResult> AddComment([FromBody] Comment comment)
        {
            var result = await BlogWrapperOps.AddCommentOpsAsync(comment);

            return Ok(result);
        }

        [HttpGet("getAllComment/{Id}")]
        public async Task<IActionResult> GetAllComments([FromRoute] int Id)
        {
            var result = await BlogWrapperOps.GetAllCommentOpsAsync(Id);

            return Ok(result);
        }

        [HttpPatch("updateComment")]
        public async Task<IActionResult> UpdateComment([FromBody] Comment comment)
        {
            BaseResponseModel result = await BlogWrapperOps.UpdateCommentOpsAsync(comment);
            if (result.Status)
                return Ok(result);
            return NotFound(result);
        }


        [HttpDelete("deleteComment/{Id}")]
        public async Task<IActionResult> DeleteComment([FromRoute] int Id)
        {
            BaseResponseModel result = await BlogWrapperOps.DeleteCommentOpsAsync(Id);
            if (result.Status)
                return Ok(result);
            return NotFound(result);
        }

        [HttpPost("addBlogWrapper")]
        public async Task<IActionResult> AddBlogWrapper(BlogWrapper blogWrapper)
        {
            BaseResponseModel result = await BlogWrapperOps.AddBlogWrapperAsync(blogWrapper);
            if (result.Status)
                return Ok(result);
            return StatusCode(503, result);
        }

        [HttpDelete("RemoveBlogWrapper/{Id}")]
        public async Task<IActionResult> DeleteBlogWrapper([FromRoute] int Id)
        {
            BaseResponseModel result = await BlogWrapperOps.DeleteBlogWrapperAsync(Id);
            if (result.Status)
                return Ok(result);
            return NotFound(result);
        }

        [HttpPatch("updateBlogWrapper")]
        public async Task<IActionResult> DeleteBlogWrapper([FromBody] BlogWrapper blogWrapper)
        {
            BaseResponseModel result = await BlogWrapperOps.UpdateBlogWrapperAsync(blogWrapper);
            if (result.Status)
                return Ok(result);
            return NotFound(result);
        }

        [HttpGet("getAllLatestBlogWrappers")]
        public async Task<IActionResult> GetAllBlogWrapper([FromQuery] int startCount)
        {
            BaseResponseModel result = await BlogWrapperOps.GetBlogWrapperOpsAsync(startCount);
            if (result.Status)
                return Ok(result);
            return NotFound(result);
        }

        [HttpGet("getCompleteBlogbyPermalink/{permalink}")]
        public async Task<IActionResult> GetCompleteBlogbyPermalink([FromRoute] string permalink)
        {
            BaseResponseModel result = await BlogWrapperOps.GetCompleteBlogbyPermalinkOpsAsync(permalink);
            if (result.Status)
                return Ok(result);
            return NotFound(result);
        }

        [HttpGet("getCompleteBlogbyBlogWrapperId/{BlogWrapperId}")]
        public async Task<IActionResult> GetCompleteBlogbyBlogWrapperId([FromRoute] string BlogWrapperId)
        {
            BaseResponseModel result = await BlogWrapperOps.GetCompleteBlogbyBlogWrapperIdOpsAsync(BlogWrapperId);
            if (result.Status)
                return Ok(result);
            return NotFound(result);
        }
    }
}