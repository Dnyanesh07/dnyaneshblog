using backend.DTO;
using backend.Models;
using backend.Operations.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace backend.Contoller
{
    [ApiController]
    [Route("blog")]
    public class BlogController : ControllerBase
    {
        private readonly IBlogOps BlogOps;
        public BlogController(IBlogOps BlogOps)
        {
            this.BlogOps = BlogOps;
        }

        [HttpPost("addSingleBlog")]
        public async Task<IActionResult> AddSingleBlog([FromBody] Blog blog)
        {
            BaseResponseModel result = await BlogOps.AddSingleBlogOpsAsync(blog);
            if (result.Status)
                return Ok(result);
            return StatusCode(503, result);
        }

        [HttpDelete("removeSingleBlog/{blogId}")]
        public async Task<IActionResult> DeleteSingleBlog([FromRoute] int blogId)
        {
            BaseResponseModel result = await BlogOps.DeleteSingleBlogOpsAsync(blogId);
            if (result.Status)
                return Ok(result);
            return StatusCode(503, result);
        }

        [HttpPost("AddAllBlogs")]
        public async Task<IActionResult> AddAllBlogs([FromBody] List<Blog> blogs)
        {
            BaseResponseModel result = await BlogOps.AddAllBlogsOpsAsync(blogs);
            if (result.Status)
                return Ok(result);
            return StatusCode(503, result);
        }

        [HttpDelete("removeAllBlogs/{blogWrapperId}")]
        public async Task<IActionResult> DeleteAllBlogs([FromRoute] int blogWrapperId)
        {
            BaseResponseModel result = await BlogOps.DeleteAllBlogsOpsAsync(blogWrapperId);
            if (result.Status)
                return Ok(result);
            return StatusCode(503, result);
        }

        [HttpGet("getAllBlogs/{BlogWrapperId}")]
        public async Task<IActionResult> AddAllBlogs([FromRoute] int BlogWrapperId)
        {
            BaseResponseModel result = await BlogOps.GetAllBlogsOpsAsync(BlogWrapperId);
            if (result.Status)
                return Ok(result);
            return StatusCode(503, result);
        }

        [HttpPatch("updateAllBlogs/{BlogWrapperId}")]
        public async Task<IActionResult> UpdateAllBlogs([FromRoute] int BlogWrapperId,[FromBody] List<Blog> blogs)
        {
            BaseResponseModel result = await BlogOps.UpdateAllBlogsOpsAsync(BlogWrapperId,blogs);
            if (result.Status)
                return Ok(result);
            return StatusCode(503, result);
        }
    }
}