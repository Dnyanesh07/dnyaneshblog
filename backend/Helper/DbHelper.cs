using backend.Models;
using Microsoft.EntityFrameworkCore;

namespace backend.Helper
{
    public class DbHelper : DbContext
    {
        public DbSet<User> Users {get;set;}
        public DbSet<BlogWrapper> BlogWrappers {get;set;}
        public DbSet<Blog> Blogs {get;set;}
        public DbSet<Like> Likes {get;set;}
        public DbSet<Comment> Comments {get;set;}
        public DbSet<Category> Categories {get;set;}

        public DbHelper(DbContextOptions<DbHelper> options):base(options)
        {
            
        }
    }
}