using backend.DTO;

namespace backend.Utility
{
    public class CommonObjectMapper
    {
        public static BaseResponseModel CreateBaseResposeModel(string Message,bool Status=true, object? Data = null )
        {
            return new BaseResponseModel()
            {
                Message = Message,
                Data = Data,
                Status = Status
            };
        }
    }
}