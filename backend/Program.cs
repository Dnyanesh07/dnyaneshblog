using backend.Helper;
using backend.Operations.Classes;
using backend.Operations.Interfaces;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

string ConnectionString = builder.Configuration["ConnectionString"];


builder.Services.AddDbContext<DbHelper>((optionsBuilder) =>
{
    optionsBuilder.UseMySql(ConnectionString, ServerVersion.AutoDetect(ConnectionString));
});

builder.Services.AddScoped<IUserOperation, UserOperation>();
builder.Services.AddScoped<IBlogWrapperOps, BlogWrapperOps>();
builder.Services.AddScoped<IBlogOps, BlogOps>();
builder.Services.AddScoped<ICategoryOps, CategoryOps>();
builder.Services.AddCors(policyBuilder =>
    policyBuilder.AddDefaultPolicy(policy =>
        policy.WithOrigins("*").AllowAnyHeader().AllowAnyHeader())
);
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
}
app.UseCors();
app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
