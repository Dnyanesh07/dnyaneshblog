const URL = "https://192.168.1.7:7247"
const CATEGORIES = [
    "Dotnet",
    "Javascript",
    "NodeJs",
    "ReactJs",
    "Gaming",
    "Hacking",
  ]

export {URL, CATEGORIES}