import CardList from "../../Components/CardList/CardList";
import CategoryList from "../../Components/CategoryList/CategoryList";
import Featured from "../../Components/Featured/Featured";
import styles from "./Homepage.module.css";

const Homepage = () => {
  return (
    <div className={styles.container}>
      <Featured />
      <CategoryList />
      <div className={styles.content}>
      <CardList/>
      {/* <Menu /> */}
    </div>
    </div>
  );
};

export default Homepage;
