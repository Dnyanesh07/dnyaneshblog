import React from "react";
import styles from "./Navbar.module.css";

import { Link, Outlet } from "react-router-dom";
import AuthLinks from "../AuthLinks/AuthLinks";
import ThemeToggle from "../ThemeToggle/ThemeToggle";

const Navbar = () => {
  return (
    <div className={styles.container}>
      <div className={styles.social}>
        <img src="/facebook.png" alt="facebook" width={24} height={24} />
        <img src="/instagram.png" alt="instagram" width={24} height={24} />
        <img src="/linkedin.png" alt="linkedin" width={24} height={24} />
        <img src="/youtube.png" alt="youtube" width={24} height={24} />
      </div>
      <div className={styles.logo}>Dnyanesh</div>
      <div className={styles.links}>
        <ThemeToggle />
        <Link to="/" className={styles.link}>Homepage</Link>
        <Link to="/" className={styles.link}>Contact</Link>
        <Link to="/" className={styles.link}>About</Link>
        <AuthLinks />
      </div>
      <Outlet/>
    </div>
  );
};

export default Navbar;
