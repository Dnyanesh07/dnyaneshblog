import React, { useEffect, useState } from "react";
import styles from "./CardList.module.css";
import axios from 'axios';
import {URL} from "../../Constants"
import Card from "../Card/Card";

const CardList = () => {
  const [ posts, setPosts ] = useState([]);

  useEffect(() => {
    async function fetchdata() {
      await axios
        .get(
          `${URL}/BlogWrapper/getAllLatestBlogWrappers?startCount=0`
        )
        .then(
          (response) => {
            console.log(response);
            setPosts(response.data.data);
          },
          (error) => {
            console.log(error);
          }
        );
    }
    fetchdata();
  },[]);

  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Recent Posts</h1>
      <div className={styles.posts}>
        {posts?.map((item) => (
          <Card item={item} key={item.blogWrapperId} />
        ))}
      </div>
    </div>
  );
};

export default CardList;
