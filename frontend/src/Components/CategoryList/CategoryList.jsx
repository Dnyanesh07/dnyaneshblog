import React, { useEffect, useState } from "react";
import styles from "./CategoryList.module.css";
import { Link } from "react-router-dom";
import { URL } from "../../Constants";
import axios from "axios";

const CategoryList = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    async function getData() {
      await axios
        .get(`${URL}/Category/getAllCategories`)
        .then((response) => {
          console.log(response);
          setData(response.data.data);
        })
        .catch((error) => {
          console.log(`error in categoryList ${error}`);
        });
    }

    getData();
  }, []);

  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Popular Categories</h1>
      <div className={styles.categories}>
        {data.map((item) => (
          <Link
            to="/blog?cat=style"
            className={`${styles.category} ${styles[item.title]}`}
            key={item.categoryId}
          >
            {item.title}
          </Link>
        ))}
      </div>
    </div>
  );
};

export default CategoryList;
