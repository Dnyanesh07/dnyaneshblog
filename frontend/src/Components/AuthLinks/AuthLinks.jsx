
import { Link } from "react-router-dom";
import styles from "./AuthLinks.module.css";
import { useState } from "react";
import { signOut, useSession } from "react";

const AuthLinks = () => {
  const [open, setOpen] = useState(false);

  // const { status } = useSession();
  const status = "unauthenticated"

  return (
    <>
      {status === "unauthenticated" ? (
        <Link to="/login" className={styles.link}>
          Login
        </Link>
      ) : (
        <>
          <Link to="/write" className={styles.link}>
            Write
          </Link>
          <span className={styles.link} onClick={signOut}>
            Logout
          </span>
        </>
      )}
      <div className={styles.burger} onClick={() => setOpen(!open)}>
        <div className={styles.line}></div>
        <div className={styles.line}></div>
        <div className={styles.line}></div>
      </div>
      {open && (
        <div className={styles.responsiveMenu}>
          <Link to="/">Homepage</Link>
          <Link to="/">About</Link>
          <Link to="/">Contact</Link>
          {status === "notauthenticated" ? (
            <Link to="/login">Login</Link>
          ) : (
            <>
              <Link to="/write">Write</Link>
              <span className={styles.link}>Logout</span>
            </>
          )}
        </div>
      )}
    </>
  );
};

export default AuthLinks;
