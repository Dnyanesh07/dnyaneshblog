import styles from "./Card.module.css";
import { Link } from "react-router-dom";

const Card = ({ key, item }) => {
  return (
    <div className={styles.container} key={key}>
      {item.imageUrl && (
        <div className={styles.imageContainer}>
          <Link to={`/posts/${item.slug}`}>
            <img src={item.imageUrl} alt="" fill className={styles.image} />
          </Link>
        </div>
      )}
      <div className={styles.textContainer}>
        <div className={styles.detail}>
          <span className={styles.date}>
            {item.createdAt.substring(0, 10)} -{" "}
          </span>
          <span className={styles.category}>{item.catSlug}</span>
        </div>
        <Link to={`/posts/${item.slug}`}>
          <h1>{item.title}</h1>
        </Link>
        <p className={styles.desc}>{item.metadata.substring(0, 150)}</p>
        {/* <div className={styles.desc} dangerouslySetInnerHTML={{ __html: item?.metadata?.substring(0,150) }}/> */}
        <Link to={`/posts/${item.slug}`} className={styles.link}>
          Read More...
        </Link>
      </div>
    </div>
  );
};

export default Card;
